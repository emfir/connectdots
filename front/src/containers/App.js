import React from 'react';
import {Button, Form, FormControl, InputGroup, Navbar, Tab, Table, Tabs} from "react-bootstrap";
import BookView from "./BookView";
import FiltredBooks from "./FiltredBooks";
import ListOfInCards from "../components/ListOfInCards";
import * as PropTypes from "prop-types";


class UserView extends React.Component {
    constructor({user_id}) {
        super(user_id);
        this.state = {
            user_name: '',
            title_score: [],
            title_incentive: []
        }
    }

    async componentDidMount() {
        fetch(`http://0.0.0.0:8000/app/users/${this.props.user_id}.json`)
            .then(response => response.json())
            .then(data => this.setState({user_name: data.username}))

        const scores = await fetch(`http://0.0.0.0:8000/app/user_score.json?owner=${this.props.user_id}`)
            .then(response => response.json())
            .then(data => data.results)

        const shorty = await fetch(`http://0.0.0.0:8000/app/shorty.json?owner=${this.props.user_id}`)
            .then(response => response.json())
            .then(data => data.results)


        let title_score = []

        for await (let data of scores) {
            const title = await fetch(`http://0.0.0.0:8000/app/books/${data.medium}.json`)
                .then(response => response.json())
                .then(data => data.title)

            title_score.push({title: title, score: data.score})
        }

        let title_incentive = []


        for await (let data of shorty) {
            const book_id = await fetch(data.medium)
                .then(response => response.json())
                .then(data => data.id)

            const title = await fetch(`http://0.0.0.0:8000/app/books/${book_id}.json`)
                .then(response => response.json())
                .then(data => data.title)

            title_incentive.push({title: title, short_incentive: data.short_incentive})
        }


        await this.setState({title_score: title_score, title_incentive: title_incentive})
    }

    render() {

        return (
            <div>
                <h1>User: {this.state.user_name} </h1>
                <Tabs defaultActiveKey="profile" id="uncontrolled-tab-example">
                    <Tab eventKey="home" title="scores">
                        <Table striped bordered hover>
                            <thead>
                            <tr>
                                <th>Title</th>
                                <th>Score</th>
                            </tr>
                            </thead>
                            <tbody>
                            {this.state.title_score.map(data => <tr>
                                <td>{data.title}</td>
                                <td>{data.score}</td>
                            </tr>)}
                            </tbody>
                        </Table>
                    </Tab>
                    <Tab eventKey="profile" title="incentives">
                        <Table striped bordered hover>
                            <thead>
                            <tr>
                                <th>Title</th>
                                <th>Short Incentive</th>
                            </tr>
                            </thead>
                            <tbody>
                            {this.state.title_incentive.map(data => <tr>
                                <td>{data.title}</td>
                                <td>{data.short_incentive}</td>
                            </tr>)}
                            </tbody>
                        </Table>
                    </Tab>
                </Tabs>
            </div>
        )
    }
}

UserView.propTypes = {user_id: PropTypes.any};

class App extends React.Component {
    constructor() {
        super();
        this.state = {
            book_search: 10,
            name_to_search: '',
            book_id: 3,
            shorty: []

        }
    }

    componentDidMount() {
        fetch('http://0.0.0.0:8000/app/main.json')
            .then(response => response.json())
            .then(data => data.results)
            .then(data => this.setState({shorty: data}))
    }

    onBookClick = (event) => {

        this.setState({
            book_id: parseInt(event.target.id),
            book_search: 1
        });

    }


    searchChange = (event) => {
        const searchField = event.target.value;
        this.setState({book_search: 0})
        this.setState({name_to_search: searchField})
    }

    usersClick = (event) => {
        this.setState({book_search: 10})
    }

    usersClickSecond = (event) => {
        this.setState({book_search: 2})
    }


    render() {


        let navbar = <Navbar className="bg-light justify-content-between">
            <Form inline>
                <InputGroup onChange={this.searchChange}>
                    <FormControl
                        placeholder="Search"
                        aria-label="Search"
                        aria-describedby="basic-addon1"
                    />
                </InputGroup>
                <Button variant="primary" onClick={this.usersClick}>User</Button>
                <Button variant="primary" onClick={this.usersClickSecond}>Home</Button>

            </Form>
        </Navbar>;

        let rest = <BookView book_id={this.state.book_id} clickEvent={this.onBookClick}/>;

        if (this.state.book_search === 0)
            rest = <FiltredBooks name={this.state.name_to_search} clickEvent={this.onBookClick}/>;
        else if (this.state.book_search === 2) {
            rest = <div>
                {this.state.shorty.map(data => <ListOfInCards data={data}/>)}
            </div>;
        } else if (this.state.book_search === 10) {
            rest = <div>
                <UserView user_id={1}/>
            </div>
        }


        return (
            <div>
                {navbar}
                {rest}
            </div>
        )

    }
}

export default App