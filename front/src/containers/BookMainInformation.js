import React from "react";
import {Card} from "react-bootstrap";


class BookMainInformation extends React.Component {
    constructor(props) {
        super(props);
        this.state = {book: {}}
    }


    componentDidMount() {
        fetch(this.props.book_link)
            .then(response => response.json())
            .then(data => this.setState({book: data}))

    }


    render() {


        return (
            <Card style={{width: '18rem'}} >
                <Card.Img variant="top" src={`https://robohash.org/${this.state.book.id}?200x200`}/>
                <Card.Body>
                    <Card.Title style={{height: 'auto'}}
                                onClick={this.props.clickEvent}
                                id={this.state.book.id}>
                        {this.state.book.title}
                    </Card.Title>
                    <Card.Text>
                        author : {this.state.book.author}
                    </Card.Text>
                </Card.Body>
            </Card>
        )
    }


}

export default BookMainInformation