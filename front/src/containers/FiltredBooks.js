import React from "react";
import BookMainInformation from "./BookMainInformation";


class FiltredBooks extends React.Component {
    constructor(props) {
        super(props);
        this.books_link = 'http://0.0.0.0:8000/app/books'
        this.state = {books: []}
    }

    componentDidMount() {
        fetch('http://0.0.0.0:8000/app/books.json')
            .then(response => response.json())
            .then(data => data.results)
            .then(data => {
                this.setState({books: data})
            })
    }


    render() {

        return (
            <div className='d-flex flex-wrap'>
                {this.state.books
                    .filter(data => data.title.toLowerCase().includes(this.props.name.toLowerCase()))
                    .map(data =>
                    <BookMainInformation
                        book_link={this.books_link + "/" + data.id + ".json"}
                        clickEvent={this.props.clickEvent}
                    />)}
            </div>
        )
    }

}

export default FiltredBooks