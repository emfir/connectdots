import React from "react";
import {BrowserRouter as Router, Link, Route, Switch} from "react-router-dom";
import {CardDeck, Nav, Navbar} from "react-bootstrap";
import InCard from "../components/InCard";
import BookMainInformation from "./BookMainInformation";


class BookAdditionalInformation extends React.Component {
    constructor(props) {
        super(props);
        this.state = {linked_books: []}
        this.base_link_to_books = 'http://0.0.0.0:8000/app/books/'
    }

    componentDidMount() {
        fetch("http://0.0.0.0:8000/app/similarity/" + this.props.book_id + ".json")
            .then(response => response.json())
            .then(data => data
                .map(data1 => data1.id)
                .map(id => this.base_link_to_books + id + ".json"))
            .then(data => this.setState({linked_books: data}))

    }

    render() {

        return (
            <Router>
                <div>
                    <Navbar bg="light" expand="lg">
                        <Navbar.Toggle aria-controls="basic-navbar-nav"/>
                        <Navbar.Collapse id="basic-navbar-nav">
                            <Nav className="mr-auto">
                                <Nav.Link><Link to="/shorty">Shorty</Link></Nav.Link>
                                <Nav.Link><Link to="/links">Links</Link></Nav.Link>

                            </Nav>


                        </Navbar.Collapse>

                    </Navbar>
                    <Switch>
                        <Route path="/shorty">
                            {this.props.shorty.map(data =>
                                <CardDeck>
                                    <InCard data={data}
                                            key={data.id}/>
                                </CardDeck>
                            )}
                        </Route>
                        <Route path="/links">
                            <div className='d-flex flex-wrap'>
                                {this.state.linked_books.map(data =>
                                    <BookMainInformation book_link={data}  clickEvent={this.props.clickEvent}/>)}
                            </div>
                        </Route>
                    </Switch>
                </div>
            </Router>
        )
    }

}


export default BookAdditionalInformation