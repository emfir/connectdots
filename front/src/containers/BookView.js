import React from 'react'
import BookMainInformation from "./BookMainInformation";
import BookAdditionalInformation from "./BookAdditionalInforamtion";


class BookView extends React.Component {
    constructor(props) {
        super(props);
        this.state = {shorty: []}
    }

    componentDidMount() {


        fetch('http://0.0.0.0:8000/app/shorty.json?medium=' + this.props.book_id)
            .then(response => response.json())
            .then(data => data.results)
            .then(data => {
                this.setState({shorty: data})
            })
    }

    render() {

        return (
            <div>
                <BookMainInformation book_link={"http://0.0.0.0:8000/app/books/" + this.props.book_id + ".json"}
                                     clickEvent={this.props.clickEvent}/>
                <BookAdditionalInformation shorty={this.state.shorty}
                                           book_id={this.props.book_id}
                                           clickEvent={this.props.clickEvent}/>
            </div>
        )
    }
}

export default BookView