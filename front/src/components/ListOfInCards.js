import React from 'react'
import {Button, Card} from "react-bootstrap";
import BookMainInformation from "../containers/BookMainInformation";

class ListOfInCards extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            like: 0,
            dislike: 0,
            book_id: 1
        }
    }

    async componentDidMount() {
        fetch(this.props.data.medium)
            .then(response => response.json())
            .then(data => this.setState({book_id: data.id}))

        const urls = await fetch(
            `http://0.0.0.0:8000/app/badge_shorty.json?shorty=${this.props.data.id}`)
            .then(response => response.json())
            .then(data => data.results)
            .then(data => data.map(obj => obj.badge))
        const data = await urls.map(url => fetch(url))
        let object = {like: 0, dislike: 0};

        for await (let request of data) {
            const data = await request.json();
            object[data.name]++
        }
        await this.setState({
            like: object.like,
            dislike: object.dislike
        })

        // await console.log(data)
    }

    render() {
        console.log(this.state.book_id)
        return (
            <Card text="light" className="mb-2" bg="secondary" border="primary" style={{width: 'auto'}}>
                {/*<BookMainInformation*/}
                {/*    book_link={`http://0.0.0.0:8000/app/books/${this.state.book_id}.json`}/>*/}
                <Card.Body>
                    <Card.Text>
                        {this.props.data.short_incentive}
                    </Card.Text>
                    <Button style={{width: '8rem'}} variant="primary" key={1}>
                        Like {this.state.like}</Button>
                    <Button style={{width: '8rem'}} variant="primary" key={2}>
                        Dislike {this.state.dislike}</Button>
                    <Card.Footer>author: {this.props.data.owner}</Card.Footer>
                </Card.Body>
            </Card>
        )
    }

}

export default ListOfInCards