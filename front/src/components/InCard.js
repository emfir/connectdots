import React from 'react'
import {Button, Card} from "react-bootstrap";

class InCard extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            like: 0,
            dislike: 0
        }
    }

    async componentDidMount() {


        const urls = await fetch(
            `http://0.0.0.0:8000/app/badge_shorty.json?shorty=${this.props.data.id}`)
            .then(response => response.json())
            .then(data => data.results)
            .then(data => data.map(obj => obj.badge))
        const data = await urls.map(url => fetch(url))
        let object = {like: 0, dislike: 0};

        for await (let request of data) {
            const data = await request.json();
            object[data.name]++
        }
        await this.setState({
            like: object.like,
            dislike: object.dislike
        })

        // await console.log(data)
    }

    render() {
        return (
            <Card text="light" className="mb-2" bg="secondary" border="primary" style={{ width: '18rem' }}>
                <Card.Body>
                    <Card.Text>
                        {this.props.data.short_incentive}
                    </Card.Text>
                    <Button style={{width: '8rem'}} variant="primary" key={1}>
                        Like {this.state.like}</Button>
                    <Button style={{width: '8rem'}} variant="primary" key={2}>
                        Dislike {this.state.dislike}</Button>
                    <Card.Footer>author: {this.props.data.owner}</Card.Footer>
                </Card.Body>
            </Card>
        )
    }

}

export default InCard