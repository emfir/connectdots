from django.contrib import admin
from django.urls import path, include
from rest_framework.routers import DefaultRouter
from rest_framework.urlpatterns import format_suffix_patterns

from .views.Color import ColorViewSet, UserColorViewSet
from .views.Link import LinkViewSet
from .views.MainPageViewSet import MainPageViewSet
from .views.Medium import MediumViewSet, BooksViewSet
from .views.Score import UserScoreViewSet, ScoreViewSet
from .views.Similarity import SimilarityViewSet
from .views.User import UserViewSet
from .views.Incentives import BadgeShortyViewSet, BadgeViewSet, ShortyViewSet

router = DefaultRouter()
router.register(r'user_score', UserScoreViewSet)
router.register(r'main', MainPageViewSet, basename='ln-languages')
router.register(r'similarity', SimilarityViewSet, basename='ln-similarity')
router.register(r'score', ScoreViewSet)
router.register(r'medium', MediumViewSet)
router.register(r'users', UserViewSet)
router.register(r'links', LinkViewSet)
router.register(r'color', ColorViewSet)
router.register(r'user_color', UserColorViewSet)
router.register(r'shorty', ShortyViewSet)
router.register(r'badge', BadgeViewSet)
router.register(r'books', BooksViewSet)

router.register(r'badge_shorty', BadgeShortyViewSet)

urlpatterns = [
    path('api-auth/', include('rest_framework.urls')),
    path('', include(router.urls)),
]
