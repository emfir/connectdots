from django.contrib.auth.models import User
from rest_framework import viewsets
from rest_framework.permissions import AllowAny

from ..serializers.User import UserSerializer


class UserViewSet(viewsets.ModelViewSet):
    # TODO: add user creation

    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = (AllowAny,)