from rest_framework import viewsets, permissions

from ..models.Incentives import Badge, Shorty, BadgeShorty
from ..permissions import IsOwnerOrReadOnly
from ..serializers.Incentives import BadgeSerializer, ShortySerializer, BadgeShortySerializer


class BadgeViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Badge.objects.all()
    serializer_class = BadgeSerializer


class ShortyViewSet(viewsets.ModelViewSet):
    queryset = Shorty.objects.all()
    serializer_class = ShortySerializer

    permission_classes = [permissions.IsAuthenticatedOrReadOnly,
                          IsOwnerOrReadOnly]

    filterset_fields = ['medium', 'owner']

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)


class BadgeShortyViewSet(viewsets.ModelViewSet):
    queryset = BadgeShorty.objects.all()
    serializer_class = BadgeShortySerializer

    permission_classes = [permissions.IsAuthenticatedOrReadOnly,
                          IsOwnerOrReadOnly]

    filterset_fields = ['shorty']

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)
