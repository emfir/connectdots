import functools

from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.serializers import ModelSerializer

from ..models.Incentives import Shorty
from ..serializers.Incentives import ShortySerializer
from rest_framework.reverse import reverse


class SnippetSerializer(ModelSerializer):
    class Meta:
        model = Shorty
        fields = "__all__"


def pagination_page(func):
    """Make sure user is logged in before proceeding"""

    @functools.wraps(func)
    def wrapper_pagination_page(*args, **kwargs):

        res = func(*args, **kwargs).data
        page = int(args[1].query_params.get('page', 0))
        links = {"next": reverse(args[0].basename + "-list",
                                 request=args[1]) + "?page={}".format(page + 1)}

        if not page:
            links["previous"] = None
        else:
            links["previous"] = reverse(args[0].basename + "-list",
                                        request=args[1]) + "?page={}".format(page + 1)

        return Response({
            'links': {
                'next': links['next'],
                'previous': links['previous']
            },
            'count': len(func(*args, **kwargs).data),
            'results': func(*args, **kwargs).data
        })

    return wrapper_pagination_page


class MainPageViewSet(viewsets.ViewSet):
    iterator = Shorty.get_iterator()
    size_of_page = 10

    @pagination_page
    def list(self, request, format=None, **kwargs):
        queryset = []
        position = int(request.query_params.get('page', 0))
        self.iterator.set_position(self.size_of_page * position)
        iterator = iter(self.iterator)

        for x in range(self.size_of_page):
            try:
                queryset.append(next(iterator))
            except:
                pass

        serializer = ShortySerializer(queryset, many=True, context={'request': request})

        return Response(serializer.data)
