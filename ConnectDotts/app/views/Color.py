from rest_framework import generics, permissions

from ..models.UserColor import UserColor
from ..permissions import IsOwnerOrReadOnly
from ..serializers.Color import ColorSerializer, UserColorSerializer
from ..models.Color import Color
from rest_framework import viewsets


class ColorViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Color.objects.all()
    serializer_class = ColorSerializer


class UserColorViewSet(viewsets.ModelViewSet):
    queryset = UserColor.objects.all()
    serializer_class = UserColorSerializer

    permission_classes = [permissions.IsAuthenticatedOrReadOnly,
                          IsOwnerOrReadOnly]

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)