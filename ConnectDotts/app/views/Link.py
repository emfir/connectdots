from rest_framework import viewsets

from ..models.Link import Link
from ..serializers.Link import LinkSerializer


class LinkViewSet(viewsets.ModelViewSet):
    queryset = Link.objects.all()
    serializer_class = LinkSerializer
