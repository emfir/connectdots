from rest_framework import viewsets

from ..models.Medium import Medium, Books
from ..serializers.Medium import MediumSerializer, BooksSerializer


class MediumViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Medium.objects.all()
    serializer_class = MediumSerializer


class BooksViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Books.objects.all()
    serializer_class = BooksSerializer
