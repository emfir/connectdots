from rest_framework import permissions, viewsets
from ..models import Score
from ..models.UserScore import UserScore
from ..permissions import IsOwnerOrReadOnly
from ..serializers.Score import UserScoreSerializer, ScoreSerializer


class UserScoreViewSet(viewsets.ModelViewSet):
    queryset = UserScore.objects.all()
    serializer_class = UserScoreSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly,
                          IsOwnerOrReadOnly]

    filterset_fields = [ 'owner']

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)


class ScoreViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Score.objects.all()
    serializer_class = ScoreSerializer
    filterset_fields = ['id']
