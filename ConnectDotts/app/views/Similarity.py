from rest_framework import viewsets
from rest_framework.response import Response

from ..models import Books
from ..serializers.Medium import BooksSerializer
from ..service.instances import similarity_factory


class SimilarityViewSet(viewsets.ViewSet):
    similarity_finder = similarity_factory.get_similarity_finder(query_set=Books.objects)

    def retrieve(self, request, pk=None, format=None, **kwargs):
        similar_books = self.similarity_finder.give_me_connected_instances(pk)
        serializer = BooksSerializer(similar_books, many=True, context={'request': request})

        return Response(serializer.data)
