from django.db.models.signals import post_save

from .models import UserColor, Color
from .models.UserScore import UserScore
from django.dispatch import receiver
from .models.Medium import Medium
from .models.Score import Score
from django.db.models import Avg, Count


@receiver(post_save, sender=UserScore)
def my_callback(sender, signal, instance, **kwargs):
    medium = Medium.objects.get(id=instance.medium_id)
    score = Score.objects.get(id=medium.score_id)
    avg = round(UserScore.objects.filter(medium=medium).aggregate(Avg('score'))['score__avg'], 2)
    score.score = avg
    score.save()
    print("New score {}".format(score.score))


@receiver(post_save, sender=UserColor)
def update_medium_color(sender, signal, instance, **kwargs):
    medium = Medium.objects.get(id=instance.medium_id)
    color = Color.objects.get(id=medium.color_id)
    rgb_color_field = UserColor.objects.filter(medium=medium).values('rgb_color_field').annotate(count=Count('rgb_color_field')).order_by(
        "count")[0]['rgb_color_field']
    color.rgb_color_field = rgb_color_field
    color.save()

    print("New color {}".format(color.rgb_color_field))