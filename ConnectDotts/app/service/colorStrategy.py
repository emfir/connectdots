from abc import ABC, abstractmethod


class ColorStrategy(ABC):

    @abstractmethod
    def calculate_color(self):
        pass