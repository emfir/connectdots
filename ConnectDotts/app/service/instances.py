from .ConcreteSimilarityFinder import ConcreteBooksSimilarityFinder, ConcreteSimilarityStrategy


class Factory:

    @staticmethod
    def get_similarity_finder(query_set):
        return ConcreteBooksSimilarityFinder(query_set,
                                             ConcreteSimilarityStrategy.get_instance())


similarity_factory = Factory
