import abc


class SimilarityStrategy(abc.ABC):
    __instance = None

    @classmethod
    def get_instance(cls):
        """ Static access method. """
        if cls.__instance is None:
            cls.__instance = cls()
        return cls.__instance

    def __init__(self):
        """ Virtually private constructor. """
        if self.__instance is not None:
            raise Exception("This class is a singleton!")
        else:
            self.__instance = self

    @abc.abstractmethod
    def judge_instance(self, instance, similar_to):
        pass


class SimilarityFinder(abc.ABC):
    def __init__(self):
        pass

    @abc.abstractmethod
    def give_me_connected_instances(self, id):
        pass
