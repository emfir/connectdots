from .SimilarityFinder import SimilarityStrategy, SimilarityFinder


class ConcreteBooksSimilarityFinder(SimilarityFinder):

    def __init__(self, query_set, similarity_strategy: SimilarityStrategy):
        super().__init__()
        self.query_set = query_set
        self.similarity_strategy = similarity_strategy

    def give_me_connected_instances(self, similar_to_this_element_id):
        similar_to_this_element = self.query_set.get(id=similar_to_this_element_id)
        potentially_similar = self.query_set.filter(id__in=
                                                    similar_to_this_element.connected_mediums
                                                    .values('id')
                                                    .distinct())

        score_table = {}

        for x in potentially_similar:
            score_table[x] = self.similarity_strategy.judge_instance(x, similar_to_this_element)

        return [k for k, v in sorted(score_table.items(),
                                     key=lambda item: item[1],
                                     reverse=True)][:3]


class ConcreteSimilarityStrategy(SimilarityStrategy):

    def judge_instance(self, instance, similar_to):
        return similar_to.connected_mediums.filter(id=instance.id).count()

    def __init__(self):
        super().__init__()
