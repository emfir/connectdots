from django.db import models

from ..models.Medium import Medium


class Link(models.Model):
    medium_one = models.ForeignKey(Medium, on_delete=models.PROTECT, related_name='from_medium')
    medium_two = models.ForeignKey(Medium, on_delete=models.PROTECT, related_name='to_medium')


class UserLink(Link):
    owner = models.ForeignKey('auth.User', on_delete=models.PROTECT)