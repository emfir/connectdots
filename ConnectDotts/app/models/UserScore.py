from django.db import models

from .Medium import Medium


class UserScore(models.Model):
    score = models.IntegerField()
    medium = models.ForeignKey(Medium, on_delete=models.PROTECT)
    owner = models.ForeignKey('auth.User', related_name='user_score', on_delete=models.PROTECT)
