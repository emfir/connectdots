from django.db import models
from .Medium import Medium
from .Color import ColorAbstract


class UserColor(ColorAbstract):
    owner = models.ForeignKey('auth.User', on_delete=models.PROTECT)
    medium = models.ForeignKey(Medium, on_delete=models.PROTECT)
