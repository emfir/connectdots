from collections import Iterator

from django.db import models

from ..models.Color import Color
from ..models.Medium import Medium


class IteratorOverMostPopular(Iterator):
    def __init__(self):
        all_shorties = {}

        for x in BadgeShorty.objects.all():
            all_shorties[x.shorty] = 1 + all_shorties.get(x.shorty, 0)

        self.queryset = [k for k, v in sorted(all_shorties.items(),
                                              key=lambda item: item[1],
                                              reverse=True)]
        self._position = 0

    def set_position(self, position):
        self._position = position

    def __next__(self):

        try:
            value = self.queryset[self._position]
            self._position += 1
        except IndexError:
            raise StopIteration()

        return value


class Incentives(models.Model):
    owner = models.ForeignKey('auth.User', on_delete=models.PROTECT)
    medium = models.ForeignKey(Medium, on_delete=models.PROTECT, related_name='incentives')
    color = models.ForeignKey(Color, on_delete=models.PROTECT)

    class Meta:
        abstract = True


class Shorty(Incentives):
    short_incentive = models.TextField()

    @staticmethod
    def get_iterator():
        return IteratorOverMostPopular()


class Badge(models.Model):
    name = models.CharField(max_length=256)
    source = models.URLField()

    def __str__(self):
        return self.name


class BadgeShorty(models.Model):
    owner = models.ForeignKey('auth.User', on_delete=models.PROTECT)
    shorty = models.ForeignKey(Shorty, on_delete=models.PROTECT, related_name='badge_shorty')
    badge = models.ForeignKey(Badge, on_delete=models.PROTECT)

    class Meta:
        unique_together = ['owner', 'shorty', 'badge']
