from django.db import models

from ..models.Color import Color
from ..models.Score import Score


class Medium(models.Model):
    color = models.OneToOneField(Color, on_delete=models.CASCADE, null=True, blank=True)
    score = models.OneToOneField(Score, on_delete=models.CASCADE, null=False, blank=False)
    connected_mediums = models.ManyToManyField('self', through='Link', blank=True, null=True)


class Books(Medium):
    title = models.CharField(max_length=256)
    author = models.CharField(max_length=256)

    def __str__(self):
        return self.title
