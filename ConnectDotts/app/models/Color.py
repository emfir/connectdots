from django.db import models


class ColorAbstract(models.Model):
    RED = 'red'
    BLUE = 'blue'
    GREEN = 'green'
    WHITE = 'white'

    COLOR = [
        (RED, 'Red color'),
        (BLUE, 'Blue color'),
        (GREEN, 'Green color'),
        (WHITE, 'White color')
    ]

    rgb_color_field = models.CharField(max_length=32, choices=COLOR, default=WHITE)

    class Meta:
        abstract = True


class Color(ColorAbstract):
    pass
