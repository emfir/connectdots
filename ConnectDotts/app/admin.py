from django.contrib import admin
from .models.Medium import Books
from .models.Color import Color
from .models.Score import Score
from .models.Incentives import Shorty
from .models.UserScore import UserScore

# Register your models here.
admin.site.register(Books)
admin.site.register(Color)
admin.site.register(Score)
admin.site.register(Shorty)
admin.site.register(UserScore)