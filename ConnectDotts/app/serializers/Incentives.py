from rest_framework import serializers

from ..models.Incentives import Badge, Shorty, BadgeShorty


class BadgeSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Badge
        fields = ["name", "source"]


class ShortySerializer(serializers.HyperlinkedModelSerializer):
    badge_shorty = serializers.HyperlinkedRelatedField(many=True, view_name='badgeshorty-detail', read_only=True)
    owner = serializers.SlugRelatedField(slug_field='username', many=False, read_only=True)

    class Meta:
        model = Shorty
        fields = ["id", "short_incentive", "owner", "medium", "color", "badge_shorty"]


class BadgeShortySerializer(serializers.HyperlinkedModelSerializer):
    owner = serializers.SlugRelatedField(slug_field='username', many=False, read_only=True)

    class Meta:
        model = BadgeShorty
        fields = "__all__"
