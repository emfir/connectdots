from rest_framework import serializers

from ..models.Link import Link


class LinkSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Link
        fields = ['id', 'medium_one', 'medium_two']

