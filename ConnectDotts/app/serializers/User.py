from django.contrib.auth.models import User
from rest_framework import serializers


class UserSerializer(serializers.HyperlinkedModelSerializer):
    """ snippets is a reverse relationship ot hte user model
     it will not be included by default when using the ModelSerializer
      class, so we needed to add an explicit field for it. """
    user_score = serializers.HyperlinkedRelatedField(many=True, view_name='userscore-detail', read_only=True)

    class Meta:
        model = User
        fields = ['id', 'username', 'password','email', 'user_score']
        extra_kwargs = {'password': {'write_only': True}}

    def create(self, validated_data):
        password = validated_data.pop('password')
        user = User(**validated_data)
        user.set_password(password)
        user.save()
        return user
