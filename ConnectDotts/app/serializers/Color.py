from rest_framework import serializers

from ..models.Color import Color
from ..models.UserColor import UserColor


class ColorSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Color
        fields = ['id', 'rgb_color_field']


class UserColorSerializer(serializers.HyperlinkedModelSerializer):
    owner = serializers.HyperlinkedIdentityField(view_name='user-detail', many=False)

    class Meta:
        model = UserColor
        fields = ['id', 'rgb_color_field', 'owner', 'medium']
