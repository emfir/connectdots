from rest_framework import serializers

from ..models.Medium import Medium
from ..models.UserScore import UserScore
from ..models.Score import Score


class UserScoreSerializer(serializers.HyperlinkedModelSerializer):
    owner = serializers.HyperlinkedIdentityField(view_name='user-detail', many=False)
    medium = serializers.SlugRelatedField(slug_field='id', many=False, read_only=False, queryset=Medium.objects.get_queryset())

    class Meta:
        model = UserScore
        fields = ['id', 'score', 'owner', 'medium']


class ScoreSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Score
        fields = ['id', 'score']