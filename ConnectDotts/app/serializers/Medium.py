from rest_framework import serializers

from ..models.Medium import Medium, Books


class MediumSerializer(serializers.HyperlinkedModelSerializer):
    score = serializers.HyperlinkedIdentityField(many=False,
                                                 view_name='score-detail')

    color = serializers.HyperlinkedIdentityField(many=False, view_name='color-detail')

    class Meta:
        model = Medium
        fields = ['id', 'score', 'color']


class BooksSerializer(serializers.HyperlinkedModelSerializer):
    connected_mediums = serializers.SlugRelatedField(slug_field='id', many=True, read_only=True)

    class Meta:
        model = Books
        fields = ['id', 'score', 'color', 'author', 'title', 'connected_mediums']
