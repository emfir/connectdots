import os

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'ConnectDotts.settings')

import django
import random

django.setup()

import random
from app.models import *
from faker import Faker
from django.contrib.auth.models import User

fakegen = Faker()


def populate_books(N=5):
    for entry in range(N):
        color = Color.objects.create()
        title = fakegen.catch_phrase()
        author = fakegen.name()
        score = Score.objects.create(score=0)
        user_rec = Books.objects.get_or_create(title=title, author=author, score=score, color=color)[0]


def populate_users(N=5):
    for x in range(N):
        user = fakegen.unique.user_name()
        email = fakegen.ascii_free_email()
        User.objects.create_user(user, email, 'admin')


def populate_links(N=5):
    items = Books.objects.all()
    users = User.objects.all()

    for x in range(N):
        medium_one = random.choice(items)
        medium_two = random.choice(items)
        user = random.choice(users)
        UserLink.objects.get_or_create(medium_one=medium_one, medium_two=medium_two, owner=user)


def populate_user_score(N=5):
    items = Books.objects.all()
    users = User.objects.all()

    for x in range(N):
        medium = random.choice(items)
        user = random.choice(users)
        score = random.randint(1, 10)
        UserScore.objects.get_or_create(score=score, medium=medium, owner=user)


def populate_badge():
    Badge.objects.get_or_create(name="like")
    Badge.objects.get_or_create(name="dislike")


def populate_shorty(N=5):
    users = User.objects.all()
    mediums = Medium.objects.all()
    colors = Color.objects.all()

    for x in range(N):
        text = fakegen.catch_phrase()
        medium = random.choice(mediums)
        color = random.choice(colors)
        user = random.choice(users)
        Shorty.objects.get_or_create(owner=user, medium=medium,
                                     color=color, short_incentive=text)


def give_badges(N=5):
    users = User.objects.all()
    shorties = Shorty.objects.all()
    badges = Badge.objects.all()

    for x in range(N):
        while True:
            try:
                user = random.choice(users)
                shorty = random.choice(shorties)
                badge = random.choice(badges)
                BadgeShorty.objects.get_or_create(owner=user,
                                                  badge=badge,
                                                  shorty=shorty)
                break
            except:
                pass


if __name__ == '__main__':
    print("populating script!")
    populate_books(50)
    populate_users(50)
    populate_links(2000)
    populate_user_score(500)
    populate_badge()
    populate_shorty(500)
    give_badges(1500)

    print("population complete")
